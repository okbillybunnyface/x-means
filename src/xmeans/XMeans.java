
package xmeans;

import java.io.*;
import java.util.*;

public class XMeans 
{
    public static void main(String[] args) 
    {
        XMeansO xmeans = new XMeansO();
        xmeans.Run(args);
    }
}

class XMeansO
{
    public static String[] testData;
    public static double[][] 
            data, //data[n][dimensions]
            centers; //centers[cluster][dimensions]
    public static ArrayList<double[]> centerList = new ArrayList<>();
    public static ArrayList<Cluster> xList = new ArrayList<>();
    public static boolean dat;
    public static int iterations;
    public static int[] minClusters, maxClusters, actualCluster;//Keeps track of the cluster each data point is in.
    public static final double SSE_TOL = 1e-6;
    
    public XMeansO(){}
    
    public void Run(String[] args)
    {
        try
        {
            Scanner argScanner = new Scanner(args[0]);
            String inputFile = argScanner.next();
            argScanner = new Scanner(args[1]);
            iterations = argScanner.nextInt();

            //Loads the names of the data files from the input file
            parseInput(inputFile);
            
            java.text.DecimalFormat fmt = new java.text.DecimalFormat("0.00");
            for(int f = 0; f < testData.length; f++)
            {
                data = load(testData[f], f);
                xList.clear();
                centerList.clear();
                //System.out.println(testData[f] + " N: " + data.length + " D: " + data[0].length);
                double[][] centroid = new double[1][];
                centroid[0] = getCentroid(data);
                System.out.println(testData[f] + " Initial BIC: " + fmt.format(getBIC(data, centroid)));
                
                
                xList.add(new Cluster(data, centroid[0]));
                for(int i = 0; i < xList.size(); i++)
                {
                    xMeans(xList.get(i).data, xList.get(i).centroid);
                }
                
                centers = new double[centerList.size()][];
                centerList.toArray(centers);
                System.out.println(testData[f] + " Final BIC: " + fmt.format(getBIC(data, centers)));
                System.out.println(testData[f] + " K: " + centers.length);
            }
        }
        catch(FileNotFoundException e)
        {
            System.out.println("File not found.");
        }
        catch(IOException e)
        {
            System.out.println("I/O exception.");
        }
    }
    
    //Runs x-means on the data set "points" and returns the centroids
    public void xMeans(double[][] points)
    {
        xMeans(points, getCentroid(points));
    }
    
    //Runs x-means on the data set "points" and returns the centroids
    public void xMeans(double[][] points, double[] centroid)
    {
        double[][] initialCentroid = new double[1][centroid.length];
        initialCentroid[0] = centroid;
        double initialBIC = getBIC(points, initialCentroid);
        
        //Split the original cluster into two & calculate the new BIC
        double[][] finalCentroids = split(points, centroid);
        double finalBIC = getBIC(points, finalCentroids);
        //if(finalBIC > Double.MAX_VALUE) finalBIC = 0;
        //System.out.println("Points: " + points.length + " Initial: " + initialBIC + " Final: " + finalBIC);
        
        if(initialBIC < finalBIC && points.length > 2)
        {
            //Find the closest cluster to each point and calculate the population of each cluster
            int[] clusterAssignment = assignClusters(points, finalCentroids);
            int[] population = new int[2];
            for(int p = 0; p < points.length; p++)
            {
                population[clusterAssignment[p]]++;
            }
            
            //Make new arrays of points based on the populations
            double[][] aPoints = new double[population[0]][];
            double[][] bPoints = new double[population[1]][];
            
            //Fill the new arrays (clusters) up with the right points
            for(int p = 0, a = 0, b = 0; p < points.length; p++)
            {
                if(clusterAssignment[p] == 0)
                {
                    aPoints[a] = points[p];
                    a++;
                }
                else
                {
                    bPoints[b] = points[p];
                    b++;
                }
            }
            
            Cluster aCluster = new Cluster(aPoints, finalCentroids[0]);
            xList.add(aCluster);
            
            Cluster bCluster = new Cluster(aPoints, finalCentroids[1]);
            xList.add(bCluster);
        }
        else//Base Case: If the initial BIC is better, then return it
        {
            centerList.add(centroid);
        }
    }
    
    //This runs kMeans on the inputed data and outputs the final centroids.
    public double[][] kMeans(double[][] points, double[][] centroids)
    {
        int[] closestCentroids = assignClusters(points, centroids);
        double initialSSE = sumOfSquaredError(points, centroids, closestCentroids);
        double oldSSE = initialSSE;
        int i;
        for(i = 0; i < iterations; i++)
        {
            centroids = getCentroids(points, closestCentroids, centroids.length);
            closestCentroids = assignClusters(points, centroids);

            double newSSE = sumOfSquaredError(points, centroids, closestCentroids);
            if(((oldSSE - newSSE) / newSSE) <= SSE_TOL)
            {
                i = iterations;
            }

            oldSSE = newSSE;
        }
        return centroids;
    }
    
    //Returns the cluster assignment for each data point.
    public int[] assignClusters(double[][] points, double[][] centroids)
    {
        int[] output = new int[points.length];
        for(int n = 0; n < points.length; n++)
        {
            double minDistance = Double.MAX_VALUE;
            for(int c = 0; c < centroids.length; c++)
            {
                double distance = distanceSquared(points[n], centroids[c]);
                
                if(distance < minDistance)
                {
                    minDistance = distance;
                    output[n] = c;
                }
            }
        }
        return output;
    }
    
    //Returns the centroid of each cluster.
    public double[][] getCentroids(double[][] points, int[] closestCentroid, int clusters)
    {
        double[][] clusterCenters = new double[clusters][points[0].length];
        int[] clusterPop = new int[clusters];
        for(int n = 0; n < points.length; n++)
        {
            clusterPop[closestCentroid[n]]++;
        }
        
        for(int d = 0; d < points[0].length; d++)
        {
            for(int n = 0; n < points.length; n++)
            {
                clusterCenters[closestCentroid[n]][d] += points[n][d];
            }
            
            for(int c = 0; c < clusters; c++)
            {
                clusterCenters[c][d] /= clusterPop[c];
            }
        }
        
        return clusterCenters;
    }
    
    public double[] getCentroid(double[][] points)
    {
        double[] centroid = new double[points[0].length];
        for(int p = 0; p < points.length; p++)
        {
            for(int d = 0; d < points[p].length; d++)
            {
                centroid[d] += points[p][d];
            }
            
        }
        for(int d = 0; d < centroid.length; d++)
        {
            centroid[d] /= points.length;
        }
        
        return centroid;
    }
    
    //Splits a cluster, runs kMeans on the new clusters, and returns the two new centroids
    public double[][] split(double[][] points, double[] centroid)
    {
        int closest = 0;//index of point closest to centroid
        double closestDistance = distanceSquared(points[0], centroid);
        for(int p = 1; p < points.length; p++)
        {
            double distance = distanceSquared(points[p], centroid);
            if(distance < closestDistance)
            {
                closestDistance = distance;
                closest = p;
            }
        }
        
        //Creates a centroid in the location of the point closest to the original centroid and another opposite it
        double[][] centroids = new double[2][centroid.length];
        for(int d = 0; d < centroid.length; d++)
        {
            centroids[0][d] = points[closest][d];
            centroids[1][d] = 2 * centroid[d] - points[closest][d];
        }
        
        return kMeans(points, centroids);
    }
    
    public double getBIC(double[][] points, double[][] centroids)
    {
        int[] closestCentroid = assignClusters(points, centroids);
        
        //Counts the number of points in each cluster
        double[] pointsNearCentroid = new double[centroids.length];
        double[] variance = new double[centroids.length];
        for(int p = 0; p < points.length; p++)
        {
            pointsNearCentroid[closestCentroid[p]]++;
            double temp = distanceSquared(points[p], centroids[closestCentroid[p]]);
            variance[closestCentroid[p]] += temp;
        }
        for(int k = 0; k < centroids.length; k++)
        {
            //variance[k] /= (points[0].length * (points.length - 1));
            variance[k] /= (points.length - centroids.length);
        }
        
        double bic = 0;
        
        for(int k = 0; k < centroids.length; k++)
        {
            bic +=  pointsNearCentroid[k] * Math.log(pointsNearCentroid[k]) // Rn*log(Rn)
                    //- pointsNearCentroid[k] * Math.log(points.length) // -Rn*log(R) The other program doesn't have this for some reason? I commented it out.
                    - pointsNearCentroid[k] / 2 * Math.log(2 * Math.PI) // -Rn/2*log(2pi)
                    - pointsNearCentroid[k] * points[0].length / 2 * Math.log(variance[k]) // -RnM/2*log(maxlikelihood)
                    - (pointsNearCentroid[k] - 1) / 2;
                    //- points[0].length * (pointsNearCentroid[k] - 1) / 2; // -(Rn-K)/2
                    //- ((centroids.length - 1) + points[0].length * centroids.length + centroids.length) / 2 * Math.log(points.length);
        }
        
        
        return bic - ((centroids.length - 1) + points[0].length * centroids.length + centroids.length) / 2 * Math.log(points.length);
        //return bic - (centroids.length * (points[0].length - 1) / 2 * Math.log(points.length));
    }
    
    public double sumOfSquaredError(double[][] data, double[][] centers, int[] dataCluster)
    {
        double sse = 0;
        for(int n = 0; n < data.length; n++)
        {
            sse += distanceSquared(data[n], centers[dataCluster[n]]);
        }
        
        return sse;
    }
    
    //Multi-dimensioned distance squared. Precondition: a and b must have the same array length.
    public double distanceSquared(double[] a, double[] b)
    {
        double distance = 0;
        for(int d = 0; d < a.length; d++)
        {
            distance += distanceSquared(a[d], b[d]);
        }
        
        return distance;
    }
    
    //Single-dimensioned distance squared.
    public double distanceSquared(double a, double b)
    {
        return (a - b) * (a - b);
    }
    
    //Returns the names of the data files to be tested by reading them from filename.
    public void parseInput(String filename) throws FileNotFoundException, IOException
    {
        File file = new File(filename);
        FileReader fileReader = new FileReader(file);
        
        Scanner scanner = new Scanner(fileReader);
        
        Stack<String> stack = new Stack();
        int count = 0;
        
        while(scanner.hasNextLine())
        {
            stack.push(scanner.nextLine());
            count++;
        }
        
        String[] input = new String[count];
        for(int i = input.length - 1; i >= 0; i--)
        {
            input[i] = stack.pop();
        }
        
        testData = new String[input.length];
        minClusters = new int[input.length];
        maxClusters = new int[input.length];
        for(int f = 0; f < input.length; f++)
        {
            scanner = new Scanner(input[f]);
            testData[f] = scanner.next();
            if(!testData[f].contains(".dat"))
            {
                minClusters[f] = scanner.nextInt();
                maxClusters[f] = scanner.nextInt();
            }
        }
    }
    
    //Loads data points from file specified.
    public double[][] load(String filename, int index) throws FileNotFoundException, IOException 
    {
        File file = new File(filename);
        FileReader fileReader = new FileReader(file);
        Scanner rawInput = new Scanner(fileReader);

        int elements = rawInput.nextInt();
        int dimensions = rawInput.nextInt();
        dat = filename.contains(".dat");
        if(dat)
        {
            dimensions--;
            minClusters[index] = rawInput.nextInt();
            maxClusters[index] = minClusters[index];
        }
        rawInput.nextLine();
        
        double[][] input = new double[elements][dimensions];
        actualCluster = new int[elements];
        
        String temp;

        Scanner parsedInput;
        for(int n = 0; n < elements; n++)
        {
            temp = rawInput.nextLine();
            parsedInput = new Scanner(temp);
            for(int d = 0; d < dimensions; d++)
            {
                input[n][d] = parsedInput.nextDouble();
            }
            if(dat)
            {
                actualCluster[n] = parsedInput.nextInt();
            }
        }
        
        //System.out.println("Load successful.");
        
        rawInput.close();
        
        double[] value = input[0];
        boolean[] varies = new boolean[dimensions];
        for(int n = 1; n < elements; n++)
        {
            for(int d = 0; d < dimensions; d++)
            {
                if(input[n][d] != value[d])
                {
                    varies[d] = true;
                }
            }
        }
        for(int d = 0; d < varies.length; d++)
        {
            if(!varies[d])
            {
                dimensions--;
            }
        }
        double[][] output = new double[dimensions][elements];
        int skipped = 0;
        for(int d = 0; d < input[0].length; d++)
        {
            if(varies[d])
            {
                for(int n = 0; n < elements; n++)
                {
                    output[d - skipped][n] = input[n][d];
                }
            } else
            {
                skipped++;
            }
        }
        input = new double[elements][dimensions];
        for(int n = 0; n < elements; n++)
        {
            for(int d = 0; d < dimensions; d++)
            {
                input[n][d] = output[d][n];
            }
        }
        
        input = normalize(input);
        
        //System.out.println("N: " + elements + " Dimensions: " + dimensions);
        
        return input;
    }
    
    //Uses min-max normalization to move all the data points to be between 0 and 1, inclusive.
    public double[][] normalize(double[][] data)
    {
        double[][] extrema = findExtrema(data);
        double[][] output = new double[data.length][data[0].length];
        
        for(int n = 0; n < data.length; n++)
        {
            for(int d = 0; d < data[n].length; d++)
            {
                output[n][d] = (data[n][d] - extrema[0][d]) / (extrema[1][d] - extrema[0][d]);
            }
        }
        
        return output;
    }
    
    //This finds the min and max values in each dimension.
    public double[][] findExtrema(double[][] data)
    {
        //These are MUCH easier to read when they're separated into two arrays.
        double[] min = new double[data[0].length];
        double[] max = new double[data[0].length];
        for(int d = 0; d < min.length; d++)
        {
            min[d] = data[0][d];
            max[d] = data[0][d];
        }

        for(int n = 0; n < data.length; n++)
        {
            for(int d = 0; d < data[n].length; d++)
            {
                if(data[n][d] > max[d])
                {
                    max[d] = data[n][d];
                }
                if(data[n][d] < min[d])
                {
                    min[d] = data[n][d];
                }
            }
        }
        
        /*int n;
        if(data.length % 2 == 0)
        {
            n = 0;
        } else 
        {
            n = 1;
        }
        
        for(; n < data.length; n += 2)
        {
            for(int d = 0; d < data[n].length; d++)
            {
                if(data[n][d] > data[n + 1][d])
                {
                    if(data[n][d] > max[d])
                    {
                        max[d] = data[n][d];
                    }
                    if(data[n + 1][d] < min[d])
                    {
                        min[d] = data[n + 1][d];
                    }
                } else 
                {
                    if(data[n + 1][d] > max[d])
                    {
                        max[d] = data[n + 1][d];
                    }
                    if(data[n][d] < min[d])
                    {
                        min[d] = data[n][d];
                    }
                }
            }
        }*/

        double[][] extrema = new double[2][data[0].length];
        for(int d = 0; d < min.length; d++)
        {
            extrema[0][d] = min[d];
            extrema[1][d] = max[d];
        }
        
        return extrema;
    }
    
    public class Cluster
    {
        double[][] data;
        double[] centroid;
        
        public Cluster(double[][] data, double[] centroid)
        {
            this.data = data;
            this.centroid = centroid;
        }
    }
}
